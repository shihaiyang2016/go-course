# Devcloud界面展示


## 登录页面

![](./devcloud-login.png)

## 管理后台

![](./devcloud-ucenter-user.png)
![](./devcloud-ucenter-role.png)
![](./devcloud-ucenter-audit.png)

## 服务发布

![](./devcloud-mpaas-service.png)
![](./devcloud-mpaas-deploy.png)
![](./devcloud-mpaas-log.png)
![](./devcloud-mpaas-console.png)

## 服务流水线

![](./devcloud-mflow-job-edit.png)
![](./devcloud-mflow-job-run.png)
![](./devcloud-mflow-pipeline.png)
![](./devcloud-mflow-pipeline-job.png)